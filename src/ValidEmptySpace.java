public class ValidEmptySpace {
    private int startRow;
    private int startColumn;

    public ValidEmptySpace(int startRow, int startColumn) {
        this.startRow = startRow;
        this.startColumn = startColumn;
    }

    public int getStartRow() {
        return startRow;
    }

    public int getStartColumn() {
        return startColumn;
    }
}
