import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {

        PlateOrganizer plateOrganizer = new PlateOrganizer();

        List<String[]> sampleNames = new ArrayList<>();

        String[] sampleNameArray1 = {"Sample-1"};
        String[] sampleNameArray2 = {"Sample-4", "Sample-5"};
        String[] sampleNameArray3 = {"Sample-6", "Sample-3", "Sample-4"};
        String[] sampleNameArray4 = {"Sample-1", "Sample-7", "Sample-3"};
        sampleNames.add(sampleNameArray1);
        sampleNames.add(sampleNameArray2);
        sampleNames.add(sampleNameArray3);
        sampleNames.add(sampleNameArray4);

        List<String[]> reagentNames = new ArrayList<>();

        String[] reagents1 = {"Pink"};
        String[] reagents2 = {"Green" , "Purple", "Gray"};
        String[] reagents3 = {"Black" , "White", "Red"};
        String[] reagents4 = {"Sage" , "Mint", "Jade"};
        reagentNames.add(reagents1);
        reagentNames.add(reagents2);
        reagentNames.add(reagents3);
        reagentNames.add(reagents4);

        int[] numberOfReplicates = {30, 22, 14, 19};

        List<Experiment[][]> plates = plateOrganizer.organizePlate(96, sampleNames, reagentNames, numberOfReplicates);

        for (Experiment[][] plate : plates) {
            printPlates(plate);
            System.out.println("-------------------------");
        }
    }

    private static void printPlates(Experiment[][] plate) {
        for(int row = 0; row < plate.length; row++) {
            for(int column = 0; column < plate[row].length; column++)
                System.out.print(plate[row][column] + " ");
            System.out.println();
        }
    }
}
