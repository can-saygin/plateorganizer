public class Experiment implements Comparable {

    private String sampleName;
    private String reagent;

    public Experiment(String sampleName, String reagent) {
        this.sampleName = sampleName;
        this.reagent = reagent;
    }

    public String getSampleName() {
        return sampleName;
    }

    public String getReagent() {
        return reagent;
    }

    @Override
    public String toString() {
        return getSampleName() + " : " + getReagent() + " |";
    }

    @Override
    public int compareTo(Object o) {
        Experiment experiment = (Experiment) o;

        int comp = this.getSampleName().compareTo(experiment.getSampleName());

        if (comp == 0) {
            return this.getReagent().compareTo(experiment.getReagent());
        }

        return comp;
    }
}
