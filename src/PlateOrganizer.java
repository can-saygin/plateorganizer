/*
 * Author: Can Saygin
 * Date: 21/06/2018
 *
 * This program organizes the placement of experiment tubes into plates with grouping by their sample names
 * and reagents
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PlateOrganizer {

    public List<Experiment[][]> organizePlate(int plateSize, List<String[]> sampleNames, List<String[]> reagentNames, int[] numberOfReplicates) throws Exception {
        if (!areArgumentSizesValid(sampleNames, reagentNames, numberOfReplicates)) {
            throw new Exception("Invalid argument sizes for sample names, reagent names or number of replicates");
        }

        // Initialize row and column sizes
        int rowSize = 0;
        int columnSize = 0;

        if (plateSize == 96) {
            rowSize = 8;
            columnSize = 12;
        } else if (plateSize == 384) {
            rowSize = 16;
            columnSize = 24;
        }

        List<Experiment> experiments = getAllExperiments(sampleNames, reagentNames, numberOfReplicates);

        List<Experiment[][]> plates = new ArrayList<>();
        int plateIndex = 0;

        // Create required plates
        int plateRequired = getRequiredPlateNumber(experiments.size(), plateSize);
        for (int n = 0; n < plateRequired; n++) {
            Experiment[][] plate = createNewPlate(plateSize);
            plates.add(plate);
        }

        int requiredSize;

        for (int i = 0; i < experiments.size(); i++) {
            Experiment temp = experiments.get(i);

            requiredSize = (int) experiments.stream().filter(e -> e.getSampleName().equals(temp.getSampleName())).count();

            if (requiredSize < columnSize) {
                ValidEmptySpace validEmptySpace = findFirstValidEmptySpaceInRow(plates, requiredSize, columnSize);
                if (validEmptySpace != null) {
                    for (int col = validEmptySpace.getStartColumn(); col < validEmptySpace.getStartColumn() + requiredSize; col++) {
                        if (i < experiments.size()) {
                            plates.get(plateIndex)[validEmptySpace.getStartRow()][col] = experiments.get(i);
                        }

                        i++;
                    }
                    i--; // Subtract 1 from index since outer for loop will add 1
                } else {
                    plateIndex++;
                }
            }
            // For sample & reagent sets that are greater than the column size
            else {
                ValidEmptySpace validEmptySpace = findEmptySpace2D(plates, requiredSize, rowSize, columnSize);
                if (validEmptySpace != null) {
                    int row = validEmptySpace.getStartRow();
                    int col = validEmptySpace.getStartColumn();
                    for (int m = requiredSize; m > 0; m--) {
                        if (i < experiments.size()) {
                            plates.get(plateIndex)[row][col] = experiments.get(i);
                        }

                        i++;
                        if (col >= columnSize - 1) {
                            row++;
                            col = 0;
                        } else {
                            col++;
                        }
                        if (row >= rowSize - 1 && col >= columnSize - 1) {
                            plateIndex++;
                            row = 0;
                            col = 0;
                        }
                    }
                    i--; // Subtract 1 from index since outer for loop will add 1
                } else {
                    plateIndex++;
                }
            }
        }
        return plates;
    }

    // Get all experiments and sort them by first sample names and then by reagents
    private List<Experiment> getAllExperiments(List<String[]> sampleNames, List<String[]> reagents, int[] numberOfReplicates) {
        List<Experiment> experiments = new ArrayList<>();

        for (int i = 0; i < sampleNames.size(); i++) {
            for (String sampleName : sampleNames.get(i)) {
                for (String reagent : reagents.get(i)) {
                    for (int j = 0; j < numberOfReplicates[i]; j++) {
                        Experiment experiment = new Experiment(sampleName, reagent);
                        experiments.add(experiment);
                    }
                }
            }
        }

        Collections.sort(experiments);
        
        return experiments;
    }

    private int getEmptySpacesInPlate(Experiment[][] plate) {
        int emptySpaces = 0;

        for (int row = 0; row < plate.length; row++) {
            for (int col = 0; col < plate[row].length; col++) {
                if (plate[row][col] == null) {
                    emptySpaces++;
                }
            }
        }
        return emptySpaces;
    }

    private Experiment[][] createNewPlate(int plateSize) throws Exception {
        if (plateSize == 96) {
            return new Experiment[8][12];
        } else if (plateSize == 384) {
            return new Experiment[16][24];
        } else {
            throw new Exception("Invalid plate size, please enter 96 or 384 as plate size");
        }
    }

    private boolean areArgumentSizesValid(List<String[]> sampleNames, List<String[]> reagentNames, int[] numberOfReplicates) {
        return (sampleNames.size() == reagentNames.size() && reagentNames.size() == numberOfReplicates.length);
    }

    private int getRequiredPlateNumber (int experimentCount, int plateSize) {
        return (int) Math.ceil((double) experimentCount / plateSize);
    }

    private ValidEmptySpace findFirstValidEmptySpaceInRow(List<Experiment[][]> plates, int requiredEmptySpace, int columnSize) {
        for (int plateIndex = 0; plateIndex < plates.size(); plateIndex++) {
            if (getEmptySpacesInPlate(plates.get(plateIndex)) >= requiredEmptySpace && requiredEmptySpace <= columnSize) {
                for (int row = 0; row < plates.get(plateIndex).length; row++) {
                    for (int col = 0; col < plates.get(plateIndex)[row].length; col++) {
                        if (plates.get(plateIndex)[row][col] == null) {
                            if (columnSize - col >= requiredEmptySpace) {
                                return new ValidEmptySpace(row, col);
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    public ValidEmptySpace findEmptySpace2D(List<Experiment[][]> plates, int requiredEmptySpace, int rowSize, int colSize) {
        int count = 0;

        for (int plateIndex = 0; plateIndex < plates.size(); plateIndex++) {
            for (int row = 0; row < plates.get(plateIndex).length; row++) {
                for (int col = 0; col < plates.get(plateIndex)[row].length; col++) {
                    if (plates.get(plateIndex)[row][col] == null) {
                        count++;
                    }
                    else {
                        count = 0;
                    }

                    if (count == requiredEmptySpace) {
                        // Trace back to find starting indexes of the empty area
                        while (count > 1) {
                            if (col > 0) {
                                col--;
                            }
                            else {
                                if (row > 0) {
                                    row--;
                                    col = colSize - 1;
                                } else {
                                    plateIndex--;
                                    row = rowSize - 1;
                                    col = colSize - 1;
                                }
                            }
                            count--;
                        }
                        return new ValidEmptySpace(row, col);
                    }
                }
            }
        }
        return null;
    }
}
